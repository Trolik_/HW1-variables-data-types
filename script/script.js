//
// 1. Як можна оголосити змінну у Javascript?
//     У JS є три види оголошення змінних - це let, var та const.
//          var - Змінні такого типу доступні лише в межах функції, але в разі відсутності такої функції ці змінні доступні по всюди.
//              З цього випливають і їх проблеми, і потреба слідкувати, що саме зписалось у змінну на кожному єтапі виконання коду.
//          let - у let на є своя область дії блоку. Можно змінювати значення. Прийшла у використання у стандарті ECMAScript шостої версії (ES6).
//              Нововведення було направлено на підтримку філософії, згідно з якою змінні повинні існувати тільки там, де вони необхідні.
//          const - при такому виді оголешення значення змінної не може бути змінено на нове. Використовують в разі коли необхідно щоб
//              значення не змінювалось в процессі виконання коду. Так само, як і у let у const є своя область дії блоку.
//
// 2. У чому різниця між функцією prompt та функцією confirm?
//          - prompt відображає модальне вікно з двома кнопками  ок/відміна та полем вводу. При внесенні інформації в поле вводу після натискання
//              ок введений текст буде присвоено до заданої змінної. При відміні нічого присвоєно до змінної не буде і змінна отримає значення null.
//          - confirm відображає модальне вікно з двома кнопками ок/відміна та текстом питання. Після натискання на ок поверне true / після натискання
//              на відміна поверне false.
//
// 3. Що таке неявне перетворення типів? Наведіть один приклад.
//          Це автоматичне перетворення типів при виконанні компіляції коду.
//              let dogName = "Jack";
//              let dogAge = 10;
//              let dogLife = dogName + ' ' + dogAge;
//              console.log(dogLife);
//
//              let jackAge = "10";
//              let jillAge = 14;
//              let divide = jackAge / jillAge;
//              console.log(divide);

// Task #1
let admin;
let name = "Alexandr";

admin = name;

console.log(admin);

// Task #2
let days = 10;
days = days * 24 * 60 * 60;

console.log(days);

// Task #3
function getData () {

    let sleepingTime = prompt('How much time do you sleep ?', 8);

    if (sleepingTime == 1) {
        sleepingTime = (`${sleepingTime} hour`)
        return sleepingTime
    } else {
        return sleepingTime = (`${sleepingTime} hours`);
    }
}

console.log(getData());